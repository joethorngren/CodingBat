package com.dayfun.codingbat;

public class WarmupOne {

    public static boolean sleepIn(boolean weekday, boolean vacation) {
        return vacation || !weekday;
    }

    public static boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
        return aSmile && bSmile || !aSmile && !bSmile;
    }


}
