package com.day.codingbat;

import com.dayfun.codingbat.WarmupOne;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WarmupOneTest {

    @Test
    public void testSleepInWhenWeekdayAndNotOnVacation() throws Exception {
        assertFalse(WarmupOne.sleepIn(true, false));
    }

    @Test
    public void testSleepInWhenWeekdayAndOnVacation() throws Exception {
        assertTrue(WarmupOne.sleepIn(true, true));
    }

    @Test
    public void testSleepInWhenNotWeekdayAndOnVacation() throws Exception {
        assertTrue(WarmupOne.sleepIn(false, true));
    }

    @Test
    public void testMonkeyTroubleWhenNeitherMonkeyIsSmiling() throws Exception {
        assertTrue(WarmupOne.monkeyTrouble(false, false));
    }

    @Test
    public void testMonkeyTroubleWhenBothMonkiesAreSmiling() throws Exception {
        assertTrue(WarmupOne.monkeyTrouble(true, true));
    }

    @Test
    public void testMonkeyTroubleWhenMonkeyASmiling() throws Exception {
        assertFalse(WarmupOne.monkeyTrouble(true, false));
    }

    @Test
    public void testMonkeyTroubleWhenMonkeyBSmiling() throws Exception {
        assertFalse(WarmupOne.monkeyTrouble(false, true));
    }

    @Test
    public void testSleepInWhenNotWeekdayAndNotOnVacation() throws Exception {
        assertTrue(WarmupOne.sleepIn(false, false));
    }
}